const express = require('express')
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.listen(port, () => console.log(`Server is running at localhost: ${port} `))

// 1.
app.get("/home", (request, response) => {
	response.send('Hello you are in the home endpoint!')
})

// 3.
let users = []

app.get("/users", (request, response) => {
	if(request.body.username !== '')
	{
		users.push(request.body)
		response.send(`User ${request.body.username} successfuly retrieved!`)
	}
	else
	{
		response.send('Please input the name of the user!')
	}
})

// 5.
let users = []
app.delete("/delete-user", (request, response) => {
 	if(request.body.username !== '')
	{
		users.push(request.body)
		response.send(`User ${request.body.username} has been removed from the database!`)
	}
	else
	{
		response.send('Please input the name of the user!')
	}
})

// SIR EARL:
app.delete("/delete-user", (request, response) => {
	let message
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1)

			message = `User has been deleted`

			break

		} else {
			message = "User does not exist!"
		}
}
response.send(message)
})